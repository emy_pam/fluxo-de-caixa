﻿-- drop database PetShop;
CREATE SCHEMA IF NOT EXISTS `PetShop` DEFAULT CHARACTER SET utf8 ;
USE `PetShop` ;

CREATE TABLE IF NOT EXISTS `PetShop`.`Cargos` (
  `idCargos` INT NOT NULL AUTO_INCREMENT,
  `nomes_cargos` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idCargos`),
  UNIQUE INDEX `idCargos_UNIQUE` (`idCargos` ASC))
ENGINE = InnoDB;

SELECT*FROM Cargos;

INSERT INTO `PetShop`.`Cargos` (`nomes_Cargos`) VALUES ('Gerente');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Medico Veterinario');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Auxiliar Veterinario');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Banhista');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Tosador');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Recepcionista');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Motorista ');
INSERT INTO `petshop`.`Cargos` (`nomes_cargos`) VALUES ('Auxiliar Geral');

-- -----------------------------------------------------
-- Table `mydb`.`Cadastro_funcionario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Cadastro_funcionario` (
  `idCadastro_funcionario` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(30) NOT NULL,
  `Data_de_nascimento` VARCHAR(45) NOT NULL,
  `Data_de_admissao` VARCHAR(45) NOT NULL,
  `Telefone` INT(11) NOT NULL,
  `Email` VARCHAR(45) NOT NULL,
  `Endereco` VARCHAR(45) NOT NULL,
  `sexo` ENUM('Masculino', 'feminino') NOT NULL,
  `Naturalidade` VARCHAR(20) NOT NULL,
  `CPF/RG` INT(11) NOT NULL,
  `Data_de_demissao` VARCHAR(45) NOT NULL,
  `Carteira_de_trabalho` INT(11) NOT NULL,
  `permicoes_usuario` TINYINT(4) NOT NULL,
  `permicoes_adm` TINYINT(4) NOT NULL,
  `permicoes_funcionario` TINYINT(4) NOT NULL,
  `Login` VARCHAR(45) NOT NULL,
  `senha` VARCHAR (11) NOT NULL,
  `salario_recebido` DECIMAL(45,2) NOT NULL,
  `Cargos_idCargos` INT NOT NULL,
  PRIMARY KEY (`idCadastro_funcionario`),
  UNIQUE INDEX `idCadastro_funcionario_UNIQUE` (`idCadastro_funcionario` ASC),
  INDEX `fk_Cadastro_funcionario_Cargos1_idx` (`Cargos_idCargos` ASC),
  CONSTRAINT `fk_Cadastro_funcionario_Cargos1`
    FOREIGN KEY (`Cargos_idCargos`)
    REFERENCES `PetShop`.`Cargos` (`idCargos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `PetShop`.`Cadastro_funcionario` (`Nome`, `Data_de_nascimento`, `Data_de_admissao`, `Telefone`, `Email`, `Endereco`, `sexo`, `Naturalidade`, `CPF/RG`, `Data_de_demissao`, `Carteira_de_trabalho`, `permicoes_usuario`, `permicoes_adm`, `permicoes_funcionario`, `Login`, `senha`, `salario_recebido`, `Cargos_idCargos`) VALUES ('Paulo', '2001/04/10', '2018/10/02', '56728923', 'PauloSantos@Gmail.com', 'Rua Betania', 'Masculino', 'Brasileiro', '1234567891', '0000/00/00', '1234567', '1', '1', '1', 'Paulo', '1234', '2500', 1);


SELECT*FROM Cadastro_funcionario;

-- -----------------------------------------------------
-- Table `mydb`.`Fornecedores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Fornecedores` (
  `idFornecedores` INT NOT NULL AUTO_INCREMENT,
  `Nome_fornecedor` VARCHAR(25) NOT NULL,
  `Endereco` VARCHAR(45) NOT NULL,
  `CNPJ/CPF` INT NOT NULL,
  `Telefone` INT NOT NULL,
  `Email` VARCHAR(50) NOT NULL,
  `Estado` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`idFornecedores`),
  UNIQUE INDEX `idFornecedores_UNIQUE` (`idFornecedores` ASC))
ENGINE = InnoDB;




SELECT*FROM fornecedores;
-- -----------------------------------------------------
-- Table `mydb`.`Produtos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Produtos` (
  `idProdutos` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(200) NOT NULL,
  `Tipo` VARCHAR(55) NOT NULL,
  `preco` decimal (45,2),
  PRIMARY KEY (`idProdutos`),
  UNIQUE INDEX `idProdutos_UNIQUE` (`idProdutos` ASC))
ENGINE = InnoDB;

INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bebedouro Pratico Automatico Caes e Gatos Amarelo Alvorada', 'Acessorios', '29.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bebedouro Linho Le Bistro XP', 'Acessorios', '69.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Comedouro Automatico Eatwell Lite', 'Acessorios', '299');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bermuda Ortopedica Vestdog', 'Acessorios', '59');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Roupa Pos Cirurgica para Gatos Pet Med', 'Acessorios', '39');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Blusa Soft sem Manga Azul Fabrica Pet', 'Acessorios', '29');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Oculos Solar Pet Rosa AgroDog', 'Acessorios', '59');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Guarda Chuva Transparente para Caes Animalissimo', 'Acessorios', '92');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Capa de Chuva Vermelha Emporium Distripet', 'Acessorios', '49');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Capa Plush Lisa Rosa Fabrica Pet', 'Acessorios', '45');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Roupinha Pos cirurgica Castrado Macho', 'Acessorios', '54');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Furminator Medio Para Caes De Pelo Longo', 'Beleza', '149.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Furminator Pequeno Para Caes De Pelo Curto', 'Beleza', '136.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Hidratante Douxo Calm Spray Cevapor', 'Beleza', '68.34');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Spray Noxxi Shine Hairl Avert ', 'Beleza', '67.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Hidratante Douxo Seb Spray Cevapor', 'Beleza', '65.94');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Hidrapet Creme Agener', 'Beleza', '55.19');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Epiotic Spherulites Ml Virbac', 'Beleza', '55.03');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Aurigen Ourofinopor', 'Beleza', '52.11');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Locao Hidratante PSK Soft Care Pet Society ', 'Beleza', '45.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Higienizador Bucal Periovet Gel Vetnil', 'Beleza', '44.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Colonia Body Splash Glamour Pet Society', 'Beleza', '42.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Colonia Body Splash Lovely Pet Society', 'Beleza', '45.60');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Kit Escova E Creme Dental Animalissimo A', 'Beleza', '32.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Locao Oto Clean Up Soft Care Pet Society ', 'Beleza', '29.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Escova Dupla Para Caes Comfort Agrodog', 'Beleza', '31.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Adaptador Para Cinto De Seguranca Preto AMF Pet ', 'Passeio', '36.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Adaptador Para Cinto De Seguranca Azul AMF Pe', 'Passeio', '36.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cinto De Segurança Bleckmann', 'Passeio', '36.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Transbike Assento Para Caes E Gatos Tubline', 'Passeio', '118.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Capa Com Cinto De Seguranca Bleckmann ', 'Passeio', '114.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Transpet Assento One Couro Tubline', 'Passeio', '120.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Transpet Assento Cinza Tubline ', 'Passeio', '124.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Transpet Assento Rosa E Lilas Tubline ', 'Passeio', '99.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Capa Para Banco De Carro Bleckmann ', 'Passeio', '110.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Guia New Classic Caes Pequeno Porte  Preta ', 'Passeio', '119.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Guia New Classic Tres Metros Azul Flexi', 'Passeio', '98.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Guia Redonda Amortecedor Azul Sao Benedito ', 'Passeio', '25.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Corrente Enforcador Com Elo Quatro por Secenta e Dois Sao Benedito', 'Passeio', '23.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Peitoral Easy Walk Pet Safe ', 'Passeio', '73.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Focinheira Para Caes Conforto Preta Pet Med', 'Passeio', '34.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Caixa de Transporte Cat It Cabrio Cinza com Rosa Chalesco', 'Caixas de transporte e Camas', '429.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Porta Ferplast Swing Basic Branco Tam Um Ferplast ', 'Caixas de transporte e Camas', '119.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Gatton Catbed Marrom para Gatos   Medio Gatton ', 'Caixas de transporte e Camas', '169.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Pickorruchos T Bone Sea Jeans  Azul Marinho  Tam Um', 'Caixas de transporte e Camas', '89.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Fabrica Pet Europa Paris Azul Marinho Tam M ', 'Caixas de transporte e Camas', '77.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Bichinho Chic Indigo  Azul  Tam P Marca Bichinho Chic', 'Caixas de transporte e Camas', '162.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Silver Pickorruchos Rosa Tam P', 'Caixas de transporte e Camas', '89.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Almofadao My Little Monster Jeans Tam P', 'Caixas de transporte e Camas', '129.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Colchonete Fbrica Pet Munique Azul Marinho  Tam Um', 'Caixas de transporte e Camas', '38.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Cama Desmontavel Hello Pet Cafe com Tecido Tribal Tam P', 'Caixas de transporte e Camas', '299.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo American Pets Play Cat Escada  Azul Marca American Pets', 'Brinquedos', '499.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Arranhador Ferplast Napoleon Movel para Gatos', 'Brinquedos', '999.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Arranhador Chalesco Elegance para Gatos Chalesco', 'Brinquedos', '599.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bola Divertida com Ratinho iPet', 'Brinquedos', '49.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Frolicat Bolt Pet Safe', 'Brinquedos', '142.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Arranhador House Liso Amarelo e Vermelho Sao Benedito', 'Brinquedos', '145.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Osso Dura Chew File e Queijo Nylabone ', 'Brinquedos', '38.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Pelucia Macaco Chalesco', 'Brinquedos', '26.60');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Brinquedo Hamburguer Western ', 'Brinquedos', '10.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bola Dog Macica LCM ', 'Brinquedos', '15.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Bandeja Sanitaria Furba Azul para Gatos', 'Higiene', '129.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Granulado Sanitario Kelco Putz Claro ', 'Higiene', '18.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Caixa de Areia United Pets para Gatos Azul', 'Higiene', '249.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Banheiro American Pets para Gatos Sprint Corner Plus Azul', 'Higiene', '129.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Granulado Ecologico Cats Best Original para Gatos ', 'Higiene', '37.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Nobre Chale Argentina Tam P', 'Casas para cachorro', '254.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Pet Injet Plastica  Vermelho Tam Dois', 'Casas para cachorro', '109.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Pet Injet Plastica Azul Tam Dois', 'Casas para cachorro', '119.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Clicknew com Chamine  Rosa  N Cinco', 'Casas para cachorro', '699.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Nobre Chale  Espanha Tam P', 'Casas para cachorro', '129.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Guisa Pet Dog Cave com Almofada  Branco', 'Casas para cachorro', '189.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Casa Pet Injet Plastica Rosa Tam Dois', 'Casas para cachorro', '109.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Extensor para Grade Plus Tubline Preto ', 'Portoes e grade para cachorros', '59.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Porta Buddy Pet Door  Tam P', 'Portoes e grade para cachorros', '199.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Grade Braganca Stop Dog Branca para Caes Branca ', 'Portoes e grade para cachorros', '119.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('APOQUEL Dezesseis MG ', 'Medicamentos Gerais', '249.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('OSURNIA Um ML', 'Medicamentos Gerais', '159.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('APOQUEL cinco/quatro ML', 'Medicamentos Gerais', '151.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SUPLEMENTO CONDROMAX PET  TABLETES', 'Medicamentos Gerais', '159.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('VETMEDIN Um/Vite e cinco MG', 'Medicamentos Gerais', '146.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SUPLEMENTO RENAVAST GATOS Trezentos MG', 'Medicamentos Gerais', '145.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SUPLEMENTO SENIOX Mil', 'Medicamentos Gerais', '115.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIBACTERIANO DOXITRAT AGENER UNIAO Duzentos MG -Vinte e quatro CP ', 'Medicamentos Gerais', '93.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIMICROBIANO CEPAV CEFEX Mil MG', 'Medicamentos Gerais', '97.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIMICROBIANO MSD FLOTRIL Cento e conquenta MG', 'Medicamentos Gerais', '67.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('GIARDICIDA CEPAV GIARDICID PARA CAES E GATOS SUSPENSAO ', 'Medicamentos Gerais', '59.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('LIMPADOR AURICULAR VIRBAC PHISIO ANTI ODOR', 'Medicamentos Gerais', '42.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('LOCAO LIMPA ORELHA VIRBAC EPIOTIC SPHERULITES Cem ML ', 'Medicamentos Gerais', '55.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SISTEMA DE TERAPIA HOMEOPET ARTROS PARA CAES E GATOS COM ARTROSE', 'Medicamentos Gerais', '80.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SISTEMA DE TERAPIA HOMEOPET CARDIOPLUS PARA CAES E GATOS', 'Medicamentos Gerais', '81.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SISTEMA DE TERAPIA HOMEOPET CIST CONTROL PARA CAES E GATOS ', 'Medicamentos Gerais', '69.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('SISTEMA DE TERAPIA HOMEOPET DISPLASIA PARA CAES E GATOS', 'Medicamentos Gerais', '85.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ADVANTAGE PARA CAES E GATOS ACIMA DE Vinte e Cinco KG  Quatro ML', 'Medicamentos Gerais', '64.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTI INFLAMATORIO BIOVET MELOXITABS Zero/Cinco MG PARA CAES E GATOS Dez CP', 'Medicamentos Gerais', '29.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIACIDO AGENER UNIAO GAVIZ V OMEPRAZOL Vinte MG', 'Medicamentos Gerais', '24.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIBACTERIANO DOXITRAT AGENER UNIAO MG  ', 'Medicamentos Gerais', '93.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIGALACTOGENICO VIRBAC CONTRALAC Vinte MG', 'Medicamentos Gerais', '75.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIMICROBIANO CEPAV CEFEX Mil MG', 'Medicamentos Gerais', '151.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS E CARRAPATOS Cinquenta MG CAES DE Quatro/Cinco A DezKG ', 'Antipulgas', '157.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('VECTRA GATOS ', 'Antipulgas', '39.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS E CARRAPATOS  CAES Cinquenta   KG ', 'Antipulgas', '199.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS E CARRAPATOS FRONTLINE PLUS GATOS', 'Antipulgas', '55.30');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('COLEIRA ANTI PULGAS E CARRAPATOS BAYER SERESTO', 'Antipulgas', '55.35');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTI PULGAS NOVARTIS CAPSTAR Onze MG PARA CAES E GATOS ATE Onze KG', 'Antipulgas', '16.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS COMBO ADVOCATE BAYER PARA GATOS ATE Q KG Zero/Quatro', 'Antipulgas', '211.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS COMBO ADVOCATE BAYER PARA GATOS DE Quatro A Oito KG  Zero/Oito ML ', 'Antipulgas', '159.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPULGAS E CARRAPATOS VIRBAC EFFIPRO PARA CAES E GATOS SPRAY ', 'Antipulgas', '169.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('COLEIRA ANTI PULGAS E CARRAPATOS BAYER SERESTO', 'Antipulgas', '193.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('PROFENDER SPOT ON GATOS', 'Vermigus', '39.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('PROGRAM PLUS Vinte e tres / Quatrocentos e Sessenta MG CAES DE Vinte e Tres ATE Quarenta e cinco KG', 'Vermigus', '128.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('ANTIPARASITARIO REVOLUTION CAES E GATOS ATE  ', 'Vermigus', '58.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('PROFENDER SPOT ON GATOS DE Dois/Cinco A 5KG', 'Vermigus', '48.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('PROGRAM PLUS Onze / Dozentos e Trinta MG CAES DE Doze ATE Vinte e Dois KG ', 'Vermigus', '109.90');

INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES (' Dog Show Dois/Cinco Kg', 'Racoes para cachorros', '34.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Dog Show Sete/Cinco Kg', 'Racoes para cachorros', '79.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Dog  Show Quinze Kg', 'Racoes para cachorros', '141.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Golden Dois/Cinco Kg', 'Racoes para cachorros', '29.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Golden Sete/Cinco Kg', 'Racoes para cachorros', '58.90');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Golden Quinze Kg', 'Racoes para cachorros', '107.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Max  Dois/Cinco Kg', 'Racoes para cachorros', '35.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Max Sete/Cinco Kg', 'Racoes para cachorros', '65.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Max Quinze Kg', 'Racoes para cachorros', '95.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('N&D Dois/Cinco Kg', 'Racoes para cachorros', '85.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('N&D Sete/Cinco Kg', 'Racoes para cachorros', '159.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('N&D Quinze Kg', 'Racoes para cachorros', '280.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Royal Canin Dois/Cinco Kg', 'Racoes para cachorros', '105.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Royal Canin Sete/Cinco Kg', 'Racoes para cachorros', '219.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Royal Canin Quinze Kg', 'Racoes para cachorros', '235.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Pro Plan Canin Dois/Cinco Kg', 'Racoes para cachorros', '79.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Pro Plan Sete/Cinco Kg', 'Racoes para cachorros', '198.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Pro Plan  Quinze Kg', 'Racoes para cachorros', '215.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Eukanuba Dois/Cinco Kg', 'Racoes para cachorros', '88.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Eukanuba Sete/Cinco Kg', 'Racoes para cachorros', '118.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Eukanuba Quinze Kg', 'Racoes para cachorros', '197.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Premier Dois/Cinco Kg', 'Racoes para cachorros', '81.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Premier Sete/Cinco Kg', 'Racoes para cachorros', '161.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Premier Quinze Kg', 'Racoes para cachorros', '176.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Natural Dois/Cinco Kg', 'Racoes para cachorros', '59.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Natural Sete/Cinco Kg', 'Racoes para cachorros', '119.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Natural  Quinze Kg', 'Racoes para cachorros', '153.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Nero  Dois/Cinco Kg', 'Racoes para cachorros', '20.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Nero Sete/Cinco Kg', 'Racoes para cachorros', '40.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Nero  Quinze Kg', 'Racoes para cachorros', '90.00');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Royal Canin Um/Cinco Kg', 'Racoes gatos', '80.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Royal Canin Sete/Cinco kg', 'Racoes gatos', '211.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Pro Plan  Um/Cinco Kg', 'Racoes gatos', '90.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Pro Plan Sete/Cinco kg', 'Racoes gatos', '159.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Max Um/Cinco Kg', 'Racoes gatos', '29.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Max Sete/Cinco kg', 'Racoes gatos', '75.70');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Hill\'s Um/Cinco Kg', 'Racoes gatos', '79.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Hill\'s Sete/Cinco kg', 'Racoes gatos', '279.99');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Natural Um/Cinco Kg', 'Racoes gatos', '55.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Natural Sete/Cinco kg', 'Racoes gatos', '126.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Equilibrio Um/Cinco Kg', 'Racoes gatos', '61.50');
INSERT INTO `petshop`.`produtos` (`Nome`, `Tipo`, `preco`) VALUES ('Equilibrio Sete/Cinco kg', 'Racoes gatos', '249.99');

SELECT *FROM produtos;
-- -----------------------------------------------------
-- Table `mydb`.`Estoque`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Estoque` (
  `idEstoque` INT NOT NULL AUTO_INCREMENT,
  `Data_entrga_produto` DATE NOT NULL,
  `organizacao_do_produto` VARCHAR(30) NOT NULL,
  `Tipo_de_produtoo` VARCHAR(45) NOT NULL,
  `Quantidade_produto` INT NOT NULL,
  `Observacoes` VARCHAR(45) NOT NULL,
  `Fornecedores_idFornecedores` INT NOT NULL,
  `Produtos_idProdutos` INT NOT NULL,
  PRIMARY KEY (`idEstoque`),
  UNIQUE INDEX `idEstoque_UNIQUE` (`idEstoque` ASC),
  INDEX `fk_Estoque_Fornecedores1_idx` (`Fornecedores_idFornecedores` ASC),
  INDEX `fk_Estoque_Produtos1_idx` (`Produtos_idProdutos` ASC),
  CONSTRAINT `fk_Estoque_Fornecedores1`
    FOREIGN KEY (`Fornecedores_idFornecedores`)
    REFERENCES `PetShop`.`Fornecedores` (`idFornecedores`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Estoque_Produtos1`
    FOREIGN KEY (`Produtos_idProdutos`)
    REFERENCES `PetShop`.`Produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SELECT*FROM Estoque;
-- -----------------------------------------------------
-- Table `mydb`.`Serviços`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Servicos` (
  `idServicos` INT NOT NULL AUTO_INCREMENT,
  `Tipo_servicos` VARCHAR(45) NOT NULL,
  `Valor_servico` DECIMAL NOT NULL,
  `Descricao` VARCHAR(45) NOT NULL,
  `Atendimento` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idServicos`),
  UNIQUE INDEX `idServicos_UNIQUE` (`idServicos` ASC))
ENGINE = InnoDB;

INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Vacinas', '99', 'Saude do animal', '3');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Vacinas', '99', 'Saude do animal', '3');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Vacinas', '79', 'Saude do animal', '3');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Vacinas', '95.00', 'Saude do animal', '3');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Vacinas', '115.00', 'Saude do animal', '3');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Banho', '65.00', 'Porte Pequeno', '4');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Banho', '75.00', 'Porte Medio', '4');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Banho', '85.00', 'Porte Grande', '4');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Geral', '99', 'Porte Pequeno', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Geral', '119', 'Porte Medio', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Geral', '139', 'Porte Grande', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Tesoura', '119', 'Porte Pequeno', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Tesoura', '139', 'Porte Medio', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Tesoura', '159', 'Porte Grande', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Higienica', '79', 'Porte Pequeno', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Higienica', '89', 'Porte Medio', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Tosa Higienica', '99', 'Porte Grande', '5');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Consultas', '240.00', 'Seg/ Sex Oito AM ate Dez PM', '2');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Consultas', '330.00', 'Seg/ Sex Dez PM ate Oito AM', '2');
INSERT INTO `petshop`.`servicos` (`Tipo_servicos`, `Valor_servico`, `Descricao`, `Atendimento`) VALUES ('Consultas', '300.00', 'Dom/Seg/Feriados', '2');


SELECT*FROM servicos;
-- -----------------------------------------------------
-- Table `mydb`.`Cadastro_cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Cadastro_cliente` (
  `idCadastro_cliente` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(25) NOT NULL,
  `endereco` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(25) NOT NULL,
  `sexo` VARCHAR(15) NOT NULL,
  `telefone` INT NOT NULL,
  PRIMARY KEY (`idCadastro_cliente`),
  UNIQUE INDEX `idCadastro_cliente_UNIQUE` (`idCadastro_cliente` ASC))
ENGINE = InnoDB;

INSERT INTO `PetShop`.`Cadastro_cliente` (`Nome`, `endereco`, `senha`, `sexo`, `telefone`) VALUES ('Emily', 'Rua Bento', '123456', 'Feminino', '59728923');

drop table Cadastro_cliente;

SELECT*FROM Cadastro_cliente;
-- -----------------------------------------------------
-- Table `mydb`.`Cadstro_animal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Cadastro_animal` (
  `idCadstro_animal` INT NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(25) NOT NULL,
  `Porte` VARCHAR(20) NOT NULL,
  `Raca` VARCHAR(45) NOT NULL,
  `Idade` INT NOT NULL,
  `Exigencias` VARCHAR(45) NULL,
  `observacoes` VARCHAR(45) NULL,
  `Cadastro_cliente_idCadastro_cliente` INT NOT NULL,
  PRIMARY KEY (`idCadstro_animal`),
  UNIQUE INDEX `idCadstro_animal_UNIQUE` (`idCadstro_animal` ASC),
  INDEX `fk_Cadastro_animal_Cadastro_cliente1_idx` (`Cadastro_cliente_idCadastro_cliente` ASC),
  CONSTRAINT `fk_Cadastro_animal_Cadastro_cliente1`
    FOREIGN KEY (`Cadastro_cliente_idCadastro_cliente`)
    REFERENCES `PetShop`.`Cadastro_cliente` (`idCadastro_cliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

INSERT INTO `PetShop`.`Cadastro_animal` (`Nome`, `Porte`, `Raca`, `Idade`, `Exigencias`, `observacoes`, `Cadastro_cliente_idCadastro_cliente`) VALUES ('Bidu', 'Grande', 'Golden', '5', 'nenhuma', 'nenhuma', 1);

SELECT*FROM Cadastro_animal;

-- -----------------------------------------------------
-- Table `mydb`.`Folha_de_pagamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Folha_de_pagamento` (
  `idFolha_de_pagamento` INT NOT NULL AUTO_INCREMENT,
  `Nome_funcionario` VARCHAR(45) NOT NULL,
  `sexo` VARCHAR(15) NOT NULL,
  `INSS` DECIMAL(45) NOT NULL,
  `IRRF` DECIMAL(45) NOT NULL,
  `Vale_transporte` DECIMAL(45) NOT NULL,
  `Vale_refeicao` DECIMAL(45) NOT NULL,
  `Salario_liquido` DECIMAL(45) NOT NULL,
  `Salario_bruto` DECIMAL(45) NOT NULL,
  `Atrasos_faltas` INT NULL,
  `descontos_totais` DECIMAL(45) NOT NULL,
  `Salario_base` DECIMAL(45) NOT NULL,
  `cargo_funcionario` VARCHAR(45) NOT NULL,
  `departamento_funcionario` VARCHAR(45) NOT NULL,
  `Cadastro_funcionario_idCadastro_funcionario` INT NOT NULL,
  `horas_extras` INT(100) NULL,
  PRIMARY KEY (`idFolha_de_pagamento`),
  UNIQUE INDEX `idFolha_de_pagamento_UNIQUE` (`idFolha_de_pagamento` ASC),
  INDEX `fk_Folha_de_pagamento_Cadastro_funcionario1_idx` (`Cadastro_funcionario_idCadastro_funcionario` ASC),
  CONSTRAINT `fk_Folha_de_pagamento_Cadastro_funcionario1`
    FOREIGN KEY (`Cadastro_funcionario_idCadastro_funcionario`)
    REFERENCES `PetShop`.`Cadastro_funcionario` (`idCadastro_funcionario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SELECT*FROM Folha_de_pagamento;
-- -----------------------------------------------------
-- Table `mydb`.`Agendameto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Agendameto` (
  `idAgendameto` INT NOT NULL AUTO_INCREMENT,
  `nome_servico` VARCHAR(45) NOT NULL,
  `preco_servico` INT NOT NULL,
  `forma_de_pagamento` VARCHAR(45) NOT NULL,
  `nome_cliente` INT NOT NULL,
  `Data` DATE NOT NULL,
  `Hora` DATETIME(5) NOT NULL,
  `Servicos_idServicos` INT NOT NULL,
  PRIMARY KEY (`idAgendameto`),
  UNIQUE INDEX `idAgendameto_UNIQUE` (`idAgendameto` ASC),
  INDEX `fk_Agendameto_Servicos1_idx` (`Servicos_idServicos` ASC),
  CONSTRAINT `fk_Agendameto_Servicos1`
    FOREIGN KEY (`Servicos_idServicos`)
    REFERENCES `PetShop`.`Servicos` (`idServicos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SELECT*FROM Agendameto;
-- -----------------------------------------------------
-- Table `mydb`.`Vendas diarias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `PetShop`.`Vendas_diarias` (
  `idVendas_diarias` INT NOT NULL AUTO_INCREMENT,
  `Data_da_venda` DATE NOT NULL,
  `preco_total` VARCHAR(45) NOT NULL,
  `Produtos_idProdutos` INT NOT NULL,
  `Servicos_idServicos` INT NOT NULL,
  PRIMARY KEY (`idVendas_diarias`),
  UNIQUE INDEX `idVendas_diarias_UNIQUE` (`idVendas_diarias` ASC),
  INDEX `fk_Vendas_diarias_Produtos1_idx` (`Produtos_idProdutos` ASC),
  INDEX `fk_Vendas_diarias_Servicos1_idx` (`Servicos_idServicos` ASC),
  CONSTRAINT `fk_Vendas_diarias_Produtos1`
    FOREIGN KEY (`Produtos_idProdutos`)
    REFERENCES `PetShop`.`Produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Vendas_diarias_Servicos1`
    FOREIGN KEY (`Servicos_idServicos`)
    REFERENCES `PetShop`.`Servicos` (`idServicos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SELECT*FROM Vendas_diarias;
-- -----------------------------------------------------
-- Table `mydb`.`Fluxo_de_caixa`
-- -----------------------------------------------------

CREATE TABLE IF NOT EXISTS `PetShop`.`fluxo_de_caixa` (
  `idfluxo_de_caixa` INT NOT NULL AUTO_INCREMENT,
  `data_entrada` DATE NOT NULL,
  `data_saida` DATE NOT NULL,
  `preco_entrada` DECIMAL NOT NULL,
  `preco_saida` DECIMAL NOT NULL,
  `Vendas_diarias_idVendas_diarias` INT NOT NULL,
  `Produtos_idProdutos` INT NOT NULL,
  PRIMARY KEY (`idfluxo_de_caixa`),
  UNIQUE INDEX `idfluxo_de_caixa_UNIQUE` (`idfluxo_de_caixa` ASC),
  FOREIGN KEY (`Produtos_idProdutos`)
    REFERENCES `PetShop`.`Produtos` (`idProdutos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;
  
  SELECT*FROM fluxo_de_caixa;
  