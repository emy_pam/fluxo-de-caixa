﻿namespace WindowsFormsApplication1
{
    partial class UserControl3
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ChtGrafico = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbEnable3D = new System.Windows.Forms.CheckBox();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ChtGrafico)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkOrange;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(734, 21);
            this.panel2.TabIndex = 9;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkOrange;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 354);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(734, 30);
            this.panel1.TabIndex = 10;
            // 
            // ChtGrafico
            // 
            chartArea1.AxisX.Title = "Produtos";
            chartArea1.AxisY.Title = "Unidades";
            chartArea1.Name = "ChartArea1";
            this.ChtGrafico.ChartAreas.Add(chartArea1);
            this.ChtGrafico.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.ChtGrafico.Legends.Add(legend1);
            this.ChtGrafico.Location = new System.Drawing.Point(0, 38);
            this.ChtGrafico.Name = "ChtGrafico";
            this.ChtGrafico.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.EarthTones;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series1.Legend = "Legend1";
            series1.Name = "Estoque";
            this.ChtGrafico.Series.Add(series1);
            this.ChtGrafico.Size = new System.Drawing.Size(734, 316);
            this.ChtGrafico.TabIndex = 12;
            this.ChtGrafico.Text = "ChtGrafico";
           
            // 
            // cbEnable3D
            // 
            this.cbEnable3D.AutoSize = true;
            this.cbEnable3D.BackColor = System.Drawing.Color.DarkOrange;
            this.cbEnable3D.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbEnable3D.Location = new System.Drawing.Point(0, 0);
            this.cbEnable3D.Name = "cbEnable3D";
            this.cbEnable3D.Size = new System.Drawing.Size(734, 17);
            this.cbEnable3D.TabIndex = 13;
            this.cbEnable3D.Text = "Ativar grafico 3D";
            this.cbEnable3D.UseVisualStyleBackColor = false;
            // 
            // Timer
            // 
            this.Timer.Enabled = true;
            this.Timer.Interval = 300;
           
            // 
            // UserControl3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ChtGrafico);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cbEnable3D);
            this.Name = "UserControl3";
            this.Size = new System.Drawing.Size(734, 384);
            ((System.ComponentModel.ISupportInitialize)(this.ChtGrafico)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart ChtGrafico;
        private System.Windows.Forms.CheckBox cbEnable3D;
        private System.Windows.Forms.Timer Timer;
    }
}
