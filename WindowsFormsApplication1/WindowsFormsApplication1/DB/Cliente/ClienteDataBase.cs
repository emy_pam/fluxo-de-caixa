﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Cliente
{
    class ClienteDataBase
    {

        public int Salvar(ClienteDTO dto)
        {

            string script = @"INSERT INTO Cadastro_cliente (Nome_Cliente, Rua, senha, sexo, telefone, idCadastro_cliente, CEP) 
                                   VALUES (@Nome_Cliente, @senha, @Rua ,@sexo, @telefone, @idCadastro_cliente, @CEP )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_Cliente", dto.Nome_Cliente));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("telefone", dto.telefone));
            parms.Add(new MySqlParameter("idCadastro_cliente", dto.idCadastro_cliente));
            parms.Add(new MySqlParameter("Rua", dto.Rua));
            parms.Add(new MySqlParameter("CEP", dto.CEP));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public ClienteDTO ConsultarPorendereco(string idCadastro_cliente )
        {
            string script = @"SELECT * FROM Cadastro_cliente WHERE idCadastro_cliente = @idCadastro_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idCadastro_cliente", idCadastro_cliente));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();
                dto.Nome_Cliente = reader.GetString("Nome_Cliente");
                dto.Rua = reader.GetString("Rua");
                dto.senha = reader.GetString("senha");
                dto.sexo = reader.GetString("sexo");
                dto.telefone = reader.GetString("telefone");
                dto.CEP = reader.GetString("CEP");
                dto.idCadastro_cliente = reader.GetInt32("idCadastro_cliente");

            }
            reader.Close();
            return dto;
        }


        public List<ClienteDTO> Listar()

        {
            string script = @"SELECT * FROM Cadastro_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())

            {

                ClienteDTO dto = new ClienteDTO();
                dto.Nome_Cliente = reader.GetString("Nome_Cliente");
                dto.Rua = reader.GetString("Rua");
                dto.sexo = reader.GetString("sexo");
                dto.senha = reader.GetString("senha");
                dto.telefone = reader.GetString("telefone");
                dto.CEP = reader.GetString("CEP");


                lista.Add(dto);

            }

            reader.Close();

            return lista;

        }

    }



}



