﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Produto
{
    class ProdutoDataBase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO Produtos (Nome, Tipo, preco ) 
                                   VALUES ( @Nome, @Tipo, @preco)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("preco", dto.preco));
            parms.Add(new MySqlParameter("Tipo", dto.Tipo));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(ProdutoDTO dto)
        {
            string script = @"UPDATE Produtos
                                 SET  Nome = @Nome,
                                      preco = @preco,
                                      Tipo = @Tipo,
                                      IdFornecedores = @IdFornecedores,
                                      idProdutos = @idProdutos
                               WHERE  Produto= @Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("preco", dto.preco));
            parms.Add(new MySqlParameter("Tipo", dto.Tipo));
            parms.Add(new MySqlParameter("IdFornecedores", dto.IdFornecedores));
            parms.Add(new MySqlParameter("idProdutos", dto.idProdutos));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM Produto WHERE Produtos = @Produtos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idProdutos", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public List<ProdutoDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM Produtos WHERE Nome like @Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", Nome + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.idProdutos = reader.GetInt32("idProdutos");
                dto.Nome = reader.GetString("Nome");
                dto.preco = reader.GetString("preco");
                dto.Tipo = reader.GetString("Tipo");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM Produtos";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.idProdutos = reader.GetInt32("idProdutos");
                dto.Nome = reader.GetString("Nome");
                dto.preco = reader.GetString("preco");
                dto.Tipo = reader.GetString("Tipo");



                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
