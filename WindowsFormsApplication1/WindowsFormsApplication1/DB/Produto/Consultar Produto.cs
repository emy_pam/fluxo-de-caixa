﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1
{
    public partial class Consultar_Produto : UserControl
    {
        public Consultar_Produto()
        {
            InitializeComponent();
            CarregarGrid();
        }

        public void CarregarGrid()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> dto = new List<ProdutoDTO>();
            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = dto;
        }

        private void Consultar_Produto_Load(object sender, EventArgs e)
        {


        }


        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoBusiness business = new ProdutoBusiness();
                List<ProdutoDTO> lista = business.Consultar(txtnome.Text.Trim());

                dgvProdutos.AutoGenerateColumns = false;
                dgvProdutos.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Pegadas",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Consultar(txtnome.Text);

            dgvProdutos.AutoGenerateColumns = false;
            dgvProdutos.DataSource = lista;
        }

        private void dgvProdutos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 2)
            {
                ProdutoDTO preco = dgvProdutos.Rows[e.RowIndex].DataBoundItem as ProdutoDTO;

                this.Hide();

            }
            if (e.ColumnIndex == 1)
            {
                ProdutoDTO produtoss = dgvProdutos.CurrentRow.DataBoundItem as ProdutoDTO;

                DialogResult r = MessageBox.Show("Deseja excluir o  produto ?", "Pegadas",
                                    MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Question);

                if (r == DialogResult.Yes)
                {
                    ProdutoBusiness business = new ProdutoBusiness();
                    business.Remover(produtoss.idProdutos);

                    CarregarGrid();
                }
            }
        }
    }
}

