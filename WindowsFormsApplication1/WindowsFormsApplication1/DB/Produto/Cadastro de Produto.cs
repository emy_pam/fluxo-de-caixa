﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Consultar_Produto;
using WindowsFormsApplication1.DB.Fornecedores;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1
{
    public partial class Cadastro_de_Produto : Form
    {
        public Cadastro_de_Produto()
        {
            InitializeComponent();
            CarregarCombos();
           
        }

        void CarregarCombos()
        {
            FornecedoresBusiness business = new FornecedoresBusiness();
            List<FonercedoresConsultarView> lista = business.Listar();

            FornecedoresDTO dto = new FornecedoresDTO();

            comboBoxFornecedor.ValueMember = nameof(FornecedoresDTO.idFornecedores);
            comboBoxFornecedor.DisplayMember = nameof(FornecedoresDTO.Nome_fornecedor);
            comboBoxFornecedor.DataSource = lista;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedoresDTO dto1 = comboBoxFornecedor.SelectedItem as FornecedoresDTO;
  
           ProdutoDTO dto = new ProdutoDTO();
           dto.Nome = textBoxNome.Text;
           dto.Tipo = textBoxTipo.Text;
           dto.preco = textBoxPreco.Text;
           dto.IdFornecedores = Convert.ToInt32(dto1);


            ProdutoBusiness business = new ProdutoBusiness();
           business.Salvar(dto);

            MessageBox.Show("Produto Registrado com sucesso!", "Pegadas", MessageBoxButtons.OK);

            textBoxNome.Text = "";
            textBoxTipo.Text = "";
            textBoxPreco.Text = "";




        }

        private void textBoxNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {
            FornecedoresDTO forn = comboBoxFornecedor.SelectedItem as FornecedoresDTO;

        }
    }
}
