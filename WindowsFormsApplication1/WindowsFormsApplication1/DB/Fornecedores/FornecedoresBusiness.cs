﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Fornecedores;

namespace WindowsFormsApplication1.DB.Fornecedores
{
    class FornecedoresBusiness
    {
        public int Salvar(FornecedoresDTO dto)
        {

            if (dto.Nome_fornecedor == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório.");
            }

            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email é obrigatório.");
            }

            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }

            if (dto.Estado == string.Empty)
            {
                throw new ArgumentException("Estado é obrigatório.");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }

            FornecedoresDatabase db = new FornecedoresDatabase();
            return db.Salvar(dto);

        }

        public void Remover(int idFornecedores)
        {
            FornecedoresDatabase db = new FornecedoresDatabase();
            db.Remover(idFornecedores);
        }

        public void Alterar(FornecedoresDTO dto)
        {
            FornecedoresDatabase db = new FornecedoresDatabase();
            db.Alterar(dto);
        }

        public List<FonercedoresConsultarView> Consultar(string  IdFornecedores)
        {
            FornecedoresDatabase db = new FornecedoresDatabase();
            return db.Consultar(IdFornecedores);
        }

        public List<FonercedoresConsultarView> Listar()
        {
            FornecedoresDatabase db = new FornecedoresDatabase();
            return db.Listar();
        }

    }


}
