﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Fornecedores
{
    class FornecedoresDatabase
    {
        public int Salvar(FornecedoresDTO dto)
        {
            string script = @"insert into Fornecedores ( Nome_fornecedor, Endereco, CNPJ, Telefone, Email, Estado, CEP, Cidade)
                                       values (@Nome_fornecedor, @Endereco, @CNPJ, @Telefone, @Email, @Estado, @CEP, @Cidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", dto.Nome_fornecedor));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Estado", dto.Estado));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FornecedoresDTO dto)

        {
            string script = @"UPDATE Fornecedores 
                                 SET  Nome_fornecedor = @Nome_fornecedor,
                                      Endereco  = @Endereco,
                                      CNPJ = @CNPJ,
                                      Telefone = @Telefone,
                                      Email = @Email,
                                      Estado = @Estado,
                                      Ciade = @Cidade,
                                      CEP = @CEP,
                               WHERE idFornecedores = @idFornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", dto.Nome_fornecedor));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Estado", dto.Estado));
            parms.Add(new MySqlParameter("Cidade", dto.Cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(int idFornecedores)
        {
            string script = @"DELETE FROM Fornecedores WHERE idFornecedores = @idFornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFornecedores", idFornecedores));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<FonercedoresConsultarView> Consultar(string prduto)
        {
            string script = @"SELECT * FROM Fornecedores WHERE Nome_fornecedor like @Nome_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", prduto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FonercedoresConsultarView> lista = new List<FonercedoresConsultarView>();
            while (reader.Read())
            {
                FonercedoresConsultarView dto = new FonercedoresConsultarView();
                dto.idFornecedores = reader.GetInt32("idFornecedores");
                dto.Nome_fornecedor = reader.GetString("Nome_fornecedor");
                dto.Endereco = reader.GetString("Endereco");
                dto.CNPJ = reader.GetString("CNPJ");
                dto.Telefone = reader.GetString("Telefone");
                dto.Email = reader.GetString("Email");
                dto.Estado = reader.GetString("Estado");
                dto.Cidade = reader.GetString("Cidade");
                dto.CEP = reader.GetString("CEP");

                lista.Add(dto);
                reader.Close();
            }
            reader.Close();
            return lista;

        }


        public List<FonercedoresConsultarView> Listar()
        {
            string script = @"SELECT * FROM Fornecedores CONSULTA";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FonercedoresConsultarView> lista = new List<FonercedoresConsultarView>();
            while (reader.Read())
            {
                FonercedoresConsultarView dto = new FonercedoresConsultarView();
                dto.idFornecedores = reader.GetInt32("idFornecedores");
                dto.Nome_fornecedor = reader.GetString("Nome_fornecedor");
                dto.Endereco = reader.GetString("Endereco");
                dto.CNPJ = reader.GetString("CNPJ");
                dto.Telefone = reader.GetString("Telefone");
                dto.Email = reader.GetString("Email");
                dto.Estado = reader.GetString("Estado");
                dto.Cidade = reader.GetString("Cidade");
                dto.CEP = reader.GetString("CEP");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}







    
     





