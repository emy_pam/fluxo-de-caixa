﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Fornecedores;

namespace WindowsFormsApplication1
{
    public partial class Fornecedores : UserControl
    {
        public Fornecedores()
        {
            InitializeComponent();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void Fornecedores_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FornecedoresDTO dto = new FornecedoresDTO();
            dto.idFornecedores = Convert.ToInt32(txtLogin.Text);
            dto.Nome_fornecedor = textBox10.Text;
            dto.Endereco = textBox1.Text;
            dto.Cidade = textBox5.Text;
            dto.Estado = txtestado.Text;
            dto.CEP = maskedTextBox3.Text;
            dto.Telefone = maskedTextBox2.Text;
            dto.Email = textBox2.Text;
            dto.CNPJ = maskedTextBox1.Text;
            FornecedoresDatabase DB = new FornecedoresDatabase();
            DB.Salvar(dto);

            MessageBox.Show("Fornecedor Registrado com sucesso!", "Pegadas", MessageBoxButtons.OK);
            txtestado.Text = "";
            txtLogin.Text = "";
            textBox10.Text = "";
            textBox1.Text = "";
            textBox5.Text = "";
            maskedTextBox3.Text = "";
            maskedTextBox2.Text = "";
            textBox2.Text = "";
            maskedTextBox1.Text = "";

        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
          
        }
    }
}
