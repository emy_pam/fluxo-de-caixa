﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Fornecedores
{
   public class FonercedoresConsultarView
    {
        public int idFornecedores { get; set; }

        public string Nome_fornecedor { get; set; }

        public string CNPJ { get; set; }

        public string Endereco { get; set; }

        public string Telefone { get; set; }

        public string Email { get; set; }

        public string Estado { get; set; }

        public string Cidade { get; set; }

        public string CEP { get; set; }
    }
}
