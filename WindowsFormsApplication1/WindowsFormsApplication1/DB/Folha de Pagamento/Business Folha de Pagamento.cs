﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Base.Folha_de_Pagamento
{
    class Business_Folha_de_Pagamento
    {

        public int Salvar(DTO_Folha_de_Pagamento  dto)
        {
            if (dto.Nome_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            DTO_Folha_de_Pagamento  cat = this.ConsultarPorNome(dto.Nome_funcionario);
            if (cat != null)
            {
                throw new ArgumentException("Funcionario já existe no sistema.");
            }

            DataBase_Folha_de_Pagamento  db = new Folha_de_Pagamento.DataBase_Folha_de_Pagamento ();
            return db.Salvar(dto);
        }


        public DTO_Folha_de_Pagamento  ConsultarPorNome(string nome)
        {
          DataBase_Folha_de_Pagamento  db = new Folha_de_Pagamento.DataBase_Folha_de_Pagamento ();
            return db.ConsultarPorNome(nome);
        }

        public List<DTO_Folha_de_Pagamento > Listar()
        {
            DataBase_Folha_de_Pagamento  db = new Folha_de_Pagamento.DataBase_Folha_de_Pagamento ();
            return db.Listar();
        }


    }
}

