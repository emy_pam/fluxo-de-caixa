﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Base.Folha_de_Pagamento
{
    class DataBase_Folha_de_Pagamento
    {
        public int Salvar(DTO_Folha_de_Pagamento  dto)
        {
            string script = @"INSERT INTO Folha_de_Pagamento (nm_nome_funcionario, ds_sexo, ds_inss, ds_irrf, vr_vale_refeicao, vt_vale_transporte, ds_salario_liquido, ds_salario_bruto, ds_atrasos_faltas, ds_descontos_totais, ds_cargo_funcionario, ds_departamento_funcionario, ds_horas_extras ) 
                                   VALUES (@nm_nome_funcionario, @ds_sexo, @ds_inss, @ds_irrf, @vr_vale_refeicao, @vt_vale_transporte, @ds_salario_liquido, @ds_salario_bruto, @ds_atrasos_faltas, @ds_descontos_totais, @ds_cargo_funcionario, @ds_departamento_funcionario, @ds_horas_extras)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_funcionario", dto.Nome_funcionario));
            parms.Add(new MySqlParameter("ds_salario_liquido", dto.Salario_liquido));
            parms.Add(new MySqlParameter("nm_descontos_totais", dto.descontos_totais));

            DataBase_Folha_de_Pagamento  db = new DataBase_Folha_de_Pagamento();
            return db.ExecuteInsertScriptWithPk (script, parms);
        }

        private int ExecuteInsertScriptWithPk(string script, List<MySqlParameter> parms)
        {
            throw new NotImplementedException();
        }

        public List<DTO_Folha_de_Pagamento > Listar()
        {
            string script = @"SELECT * FROM tb_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();

           DataBase_Folha_de_Pagamento  db = new Folha_de_Pagamento.DataBase_Folha_de_Pagamento ();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DTO_Folha_de_Pagamento> lista = new List<DTO_Folha_de_Pagamento>();
            while (reader.Read())
            {
                DTO_Folha_de_Pagamento dto = new DTO_Folha_de_Pagamento();
                dto.Id = reader.GetInt32("id_Categoria");
                dto.Nome_funcionario = reader.GetString("nm_nome_funcionario");
                dto.Salario_base = reader.GetDecimal("ds_salario_base");
                dto.Salario_liquido = reader.GetDecimal("ds_salario_liquido");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        private MySqlDataReader ExecuteSelectScript(string script, List<MySqlParameter> parms)
        {
            throw new NotImplementedException();
        }

        public DTO_Folha_de_Pagamento ConsultarPorNome(string nome_funcionario)
        {
            string script = @"SELECT * FROM tb_categoria WHERE nm_categoria = @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_funcionario", nome_funcionario));

           DataBase_Folha_de_Pagamento db = new Folha_de_Pagamento.DataBase_Folha_de_Pagamento ();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            DTO_Folha_de_Pagamento dto = null;
            while (reader.Read())
            {
                dto = new DTO_Folha_de_Pagamento();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome_funcionario = reader.GetString("nm_nome_funcionario");
                dto.Salario_base = reader.GetDecimal("ds_salario_base");
                dto.Salario_liquido = reader.GetDecimal("ds_dalario_liquido");
            }
            reader.Close();

            return dto;
        }
    }
}

