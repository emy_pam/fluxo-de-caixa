﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Base.Folha_de_Pagamento
{
    class DTO_Folha_de_Pagamento
    {

        public int Id { get; set; }
        public string Nome_funcionario { get; set; }
        public Decimal INSS { get; set; }
        public Decimal IRRF { get; set; }
        public Decimal Vale_trasporte { get; set; }
        public Decimal Vale_refeicao { get; set; }
        public Decimal Salario_liquido { get; set; }
        public Decimal Salario_bruto { get; set; }
        public int Atrasos_faltas { get; set; }
        public Decimal Desconto_totais { get; set; }
        public Decimal Salario_base { get; set; }
        public string cargo_funcionario { get; set; }
        public string departamento_funcionario { get; set; }
        public string sexo { get; set; }
        public Decimal descontos_totais { get; set; }
       
    }
}
