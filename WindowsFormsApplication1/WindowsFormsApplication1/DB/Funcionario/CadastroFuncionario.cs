﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Funcionario;

namespace WindowsFormsApplication1
{
    public partial class CadastroFuncionario : Form
    {
        public CadastroFuncionario()
        {
            InitializeComponent();
        }

        private void Salvar_Click(object sender, EventArgs e)
        {
            FuncionarioDTO dto = new FuncionarioDTO();
            dto.Nome = textBox2.Text;
            dto.Endereco = textBox1.Text;
            dto.Carteira_de_trabalho = maskedTextBox6.Text;
            dto.CPF = maskedTextBox1.Text;
            dto.Data_de_admissao = maskedTextBox5.Text;
            dto.Data_de_demissao = maskedTextBox4.Text;
            dto.Telefone = maskedTextBox3.Text;
            dto.Cargos = comboBox2.Text;
            dto.Email = textBox4.Text;
            dto.Naturalidade = textBox10.Text;
            dto.RG = maskedTextBox2.Text;
            dto.salario_recebido = comboBoxSalario.Text;
            dto.sexo = comboBox3.Text;
            dto.permicoes_funcionario = radioButton2.Checked;
            dto.permicoes_adm = radioButton1.Checked;
            dto.Login = textBox5.Text;
            dto.Senha = textBox3.Text;


            FuncionarioDatabase DB = new FuncionarioDatabase();
            DB.Salvar(dto);

            MessageBox.Show("Funcionário Registrado com sucesso!", "Pegadas", MessageBoxButtons.OK);

            this.Hide();
            Inicial tela = new Inicial();
            tela.Show();
            
        }

        private void CadastroFuncionario_Load(object sender, EventArgs e)
        {

        }
    }
    }

