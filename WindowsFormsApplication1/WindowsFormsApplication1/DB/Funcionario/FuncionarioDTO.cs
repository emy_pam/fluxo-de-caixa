﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Funcionario
{
    public class FuncionarioDTO
    {
       
            public int idCadastro_funcionario { get; set; }
            public string Nome { get; set; }
            public string Email { get; set; }
            public string sexo { get; set; }
            public string Data_de_nascimento { get; set; }
            public string Data_de_admissao { get; set; }
            public string Cargos { get; set; }
            public string Telefone { get; set; }
            public string Endereco { get; set; }
            public string CPF { get; set; }
            public string RG { get; set; }
            public string Data_de_demissao { get; set; }
            public string Carteira_de_trabalho { get; set; }
            public string Naturalidade { get; set; }
            public bool permicoes_usuario { get; set; }
            public bool permicoes_adm { get; set; }
            public bool permicoes_funcionario { get; set; }
           public string salario_recebido { get; set; }
           public string Login { get; set; }
           public string Senha { get; set; }




    }
}
