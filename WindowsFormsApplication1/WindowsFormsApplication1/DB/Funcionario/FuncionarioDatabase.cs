﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Funcionario
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO Cadastro_funcionario (Nome, Data_de_admissao, Telefone,Email,Endereco, sexo, Naturalidade, CPF, RG, Data_de_demissao, Carteira_de_trabalho, Login,senha,salario_recebido,permicoes_adm,permicoes_funcionario,Cargos) 
                                   VALUES (@Nome, @Data_de_admissao, @Telefone, @Email, @Endereco, @sexo, @Naturalidade, @CPF,@RG, @Data_de_demissao, @Carteira_de_trabalho , @Login,@senha,@salario_recebido, @permicoes_adm, @permicoes_funcionario,@Cargos)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Data_de_admissao", dto.Data_de_admissao));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("Naturalidade", dto.Naturalidade));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("RG", dto.RG));
            parms.Add(new MySqlParameter("Data_de_demissao", dto.Data_de_demissao));
            parms.Add(new MySqlParameter("Carteira_de_trabalho", dto.Carteira_de_trabalho));
            parms.Add(new MySqlParameter("Login", dto.Login));
            parms.Add(new MySqlParameter("senha", dto.Senha));
            parms.Add(new MySqlParameter("salario_recebido", dto.salario_recebido));
            parms.Add(new MySqlParameter("permicoes_adm", dto.permicoes_adm));
            parms.Add(new MySqlParameter("permicoes_funcionario", dto.permicoes_funcionario));
            parms.Add(new MySqlParameter("Cargos", dto.Cargos));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FuncionarioDTO dto)
        {

            string script = @"SELECT * FROM Cadastro_Funcionario
 
                                   WHERE       Nome = @Nome,
                                               salario_recebido = @salario_recebido,
                                               sexo = @sexo,
                                               Email = @Email,
                                               Data_de_adimissao = @Data_de_adimissao,
                                               Telefone = @Telefone,
                                               Endereco = @Endereco,
                                               RG =  @RG,
                                               CPF = @CPF,
                                               Carteira_de_trabalho = @Carteira_de_trabalho,
                                               Data_de_demissao = @Data_de_demissao,
                                               Cargos = @Cargos,
                                               permicoes_adm = @permicoes_adm,
                                               permicoes_funcionario = @permicoes_funcionario,
                                             AND   Naturalidade = @Naturalidade";


            List<MySqlParameter> parms = new List<MySqlParameter>();
          
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Data_de_admissao", dto.sexo));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Cargos", dto.Cargos));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("Naturalidade", dto.Carteira_de_trabalho));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("RG", dto.RG));
            parms.Add(new MySqlParameter("Data_de_demissao", dto.Data_de_demissao));
            parms.Add(new MySqlParameter("Carteira_de_trabalho", dto.Carteira_de_trabalho));
            parms.Add(new MySqlParameter("Login", dto.Login));
            parms.Add(new MySqlParameter("senha", dto.Senha));
            parms.Add(new MySqlParameter("salario_recebido", dto.salario_recebido));
            parms.Add(new MySqlParameter("permicoes_adm", dto.permicoes_adm));
            parms.Add(new MySqlParameter("permicoes_funcionario", dto.permicoes_funcionario));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }

        public void Remover(int id)
        {
            string script = @"INSERT INTO Cadastro_funcionario (idCadastro_funcionario) 
                                   VALUES (@idCadastro_funcionario)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idCadastro_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }




        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM Cadastro_funcionario";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();

                dto.idCadastro_funcionario = reader.GetInt32("idCadastro_funcionario");
                dto.Nome = reader.GetString("nome");
                dto.salario_recebido = reader.GetString("salario_recebido");
                dto.sexo = reader.GetString("sexo");
                dto.Email = reader.GetString("Email");
                dto.Data_de_admissao = reader.GetString("Data_de_admissao");
                dto.Telefone = reader.GetString("Telefone");
                dto.Endereco = reader.GetString("Endereco");
                dto.RG = reader.GetString("RG");
                dto.CPF = reader.GetString("CPF");
                dto.Carteira_de_trabalho = reader.GetString("Carteira_de_trabalho");
                dto.Data_de_demissao = reader.GetString("Data_de_demissao");
                dto.Naturalidade = reader.GetString("Naturalidade");
                dto.permicoes_adm = reader.GetBoolean("permicoes_adm");
                dto.permicoes_funcionario = reader.GetBoolean("permicoes_funcionario");
                dto.Cargos = reader.GetString("Cargos");





                lista.Add(dto);

            }
            reader.Close();
            return lista;
        }
        public FuncionarioDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM Cadastro_funcionario WHERE Login = @Login AND senha = @senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Login", login));
            parms.Add(new MySqlParameter("senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FuncionarioDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new FuncionarioDTO();

                funcionario.idCadastro_funcionario = reader.GetInt32("idCadastro_funcionario");
                funcionario.Nome = reader.GetString("nome");
                funcionario.salario_recebido = reader.GetString("salario_recebido");
                funcionario.sexo = reader.GetString("sexo");
                funcionario.Email = reader.GetString("Email");
                funcionario.Data_de_admissao = reader.GetString("Data_de_admissao");
                funcionario.Telefone = reader.GetString("Telefone");
                funcionario.Endereco = reader.GetString("Endereco");
                funcionario.RG = reader.GetString("RG");
                funcionario.CPF = reader.GetString("CPF");
                funcionario.Carteira_de_trabalho = reader.GetString("Carteira_de_trabalho");
                funcionario.Data_de_demissao = reader.GetString("Data_de_demissao");
                funcionario.Naturalidade = reader.GetString("Naturalidade");
                funcionario.permicoes_adm = reader.GetBoolean("permicoes_adm");
                funcionario.permicoes_funcionario = reader.GetBoolean("permicoes_funcionario");
                funcionario.Cargos = reader.GetString("Cargos");



            }

            reader.Close();

            return funcionario;
        }

        public List<FuncionarioDTO> Consultar(string Nome)
        {
            string script = @"SELECT * FROM Cadastro_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", Nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read()) 
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.idCadastro_funcionario = reader.GetInt32("idCadastro_funcionario");
                dto.Nome = reader.GetString("Nome");
                dto.CPF = reader.GetString("CPF");
                dto.Email = reader.GetString("Email");
                dto.Data_de_admissao = reader.GetString("Data_de_admissao");
                dto.Data_de_demissao = reader.GetString("Data_de_demissao");
                dto.Endereco = reader.GetString("Endereco");
                dto.Naturalidade = reader.GetString("Naturalidade");
                dto.RG = reader.GetString("RG");
                dto.sexo = reader.GetString("sexo");
                dto.Telefone = reader.GetString("Telefone");
                dto.permicoes_adm = reader.GetBoolean("permicoes_adm");
                dto.permicoes_funcionario = reader.GetBoolean("permicoes_funcionario");
                dto.Cargos = reader.GetString("Cargos");
                dto.Carteira_de_trabalho = reader.GetString("Carteira_de_trabalho");
                dto.salario_recebido = reader.GetString("salario_recebido");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }

}
