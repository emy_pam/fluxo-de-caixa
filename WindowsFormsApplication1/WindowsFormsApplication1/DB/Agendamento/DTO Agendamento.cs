﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Base.Agendamento
{
    class DTO_Agendamento
    {
        public int IdAgendamento { get; set; }
        public string nome_servico { get; set; }
        public string preco_servico { get; set; }
        public string forma_de_pagamento { get; set; }
        public string nome_cliente { get; set; }
        public string Data  { get; set; }
        public string Hora { get; set; }
        public int idServicos { get; set; }

    }
}
