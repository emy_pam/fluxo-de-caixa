﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Consultar_Produto
{
    class ConsultarProdutoBusiness
    {
         public int Salvar(ConsultarProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.preco <= 0)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

          ConsultarProdutoDatabase db = new ConsultarProdutoDatabase();
           return db.Salvar(dto);
        }



       
    }
}
