﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1.DB.Consultar_Produto
{
    class ConsultarProdutoDatabase
    {
        public int Salvar(ConsultarProdutoDTO dto)
        {
            string script = @"INSERT INTO Produto (idFornecedores, Nome, preco, Tipo, idProdutos) 
                                   VALUES (@idFornecedores, @Nome, @preco, @tipo, @idProdutos)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Tipo", dto.Tipo));
            parms.Add(new MySqlParameter("preco", dto.preco));
            parms.Add(new MySqlParameter("idProdutos", dto.idProdutos));
            parms.Add(new MySqlParameter("idFornecedores", dto.idFornecedores));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(ConsultarProdutoDTO dto)
        {
            string script = @"UPDATE Produto
                                 SET  Nome = @Nome,
                                      preco = @preco,
                                      Tipo = @Tipo,
                                      idProdutos = @idProdutos,
                                      idFornecedores = @idFornecedores,
                               WHERE  Produto= @Produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("preco", dto.preco));
            parms.Add(new MySqlParameter("Tipo", dto.Tipo));
            parms.Add(new MySqlParameter("idProdutos", dto.idProdutos));
            parms.Add(new MySqlParameter("IdFornecedor", dto.idFornecedores));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        
        
    }
}
