﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Consultar_Produto
{
    public class ConsultarProdutoDTO
    {
        public int idProdutos { get; set; }
        public string Nome { get; set; }
        public decimal preco { get; set; }
        public string Tipo { get; set; }
        public int idFornecedores { get; set; }

      
    }
}
