﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Objeto
{
    class FolhaPagamento
    {
        public decimal SalarioHora(decimal s_base)
        {

            decimal salarioH = s_base / 220;

            return salarioH;
        }

        public decimal ValeTransporte(decimal Salario)
        {

            decimal Vale = (Salario * 6) / 100;

            return Vale;
        }

        public decimal CalculoINSS(decimal s_bases, decimal hrExtra, decimal dsr)
        {



            decimal INSS = (s_bases + hrExtra + dsr);

            if (INSS >= 954 && INSS <= 1693.72m)
            {

                INSS = (s_bases * 8) / 100;

            }
            if (INSS >= 1693.73m && INSS <= 2822.90m)
            {

                INSS = (s_bases * 9) / 100;


            }
            if (INSS >= 2822.91m && INSS <= 5645.80m)
            {

                INSS = (s_bases * 11) / 100;


            }
            return INSS;

        }



        public decimal CalculoFGTS(decimal salarioB, decimal horaextra2, decimal dsr10)
        {

            decimal sala = (salarioB + horaextra2 + dsr10);
            decimal FGTS = (sala * 8) / 100;

            return FGTS;
        }

        public decimal HoraExtra1(decimal s_base, int horaExtra)
        {
            decimal salarioH = (s_base / 220);

            decimal hora50 = (salarioH * 50) / 100;

            decimal salario50 = (hora50 + salarioH);

            decimal total = (salario50 * horaExtra);

            return total;

        }
        public decimal HoraExtra100(decimal s_base, int horas)
        {
            decimal horas100 = SalarioHora(s_base);
            decimal hora100 = (horas100 * 2) * horas;




            return hora100;
        }

        public decimal CalculoImpostoRenda(decimal sa_base, decimal atrasoss, decimal horExtra, decimal dsr2, decimal INSS)
        {
            decimal SalarioBase = sa_base + horExtra + dsr2 - (atrasoss + INSS);

            decimal ImpostoRenda = 0;

            decimal Imposto = 0;

            if (SalarioBase <= 1903.98m)
            {

                Imposto = 0;

            }
            if (SalarioBase >= 1903.99m && SalarioBase <= 2826.65m)
            {
                ImpostoRenda = (sa_base * 7.5m) / 100;

                if (ImpostoRenda >= 142.80m)
                {

                    Imposto = 142.80m;

                }


            }
            if (SalarioBase >= 2826.66m && SalarioBase <= 3751.05m)
            {

                ImpostoRenda = (sa_base * 15) / 100;

                if (ImpostoRenda >= 354.80m)
                {

                    Imposto = 354.80m;

                }


            }
            if (SalarioBase >= 3751.06m && SalarioBase <= 4664.68m)
            {

                ImpostoRenda = (sa_base * 22.5m) / 100;

                if (ImpostoRenda >= 636.13m)
                {

                    Imposto = 636.13m;

                }

            }
            if (SalarioBase >= 4664.68m)
            {

                ImpostoRenda = (sa_base * 27.5m) / 100;

                if (ImpostoRenda >= 869.36m)
                {

                    Imposto = 869.36m;

                }


            }
            return Imposto;






        }
        public decimal CalcularAtrasos(decimal salariohora, int horas)
        {
            decimal valor = salariohora * horas;

            return valor;
        }

        public decimal CalcularDSR(decimal valorExtra, decimal faltas)
        {


            decimal falta = 26 - faltas;
            decimal dsr = (valorExtra / falta) * 4;

            return dsr;
        }

        public decimal CalcularFalta(decimal salario, int falta)
        {
            decimal faltas = (salario / 30) * falta;

            return faltas;
        }

        public decimal CalcularLiquido(decimal salarioBase, decimal horasextra10, decimal drs20, decimal inss10, decimal ir, decimal vt10)
        {
            decimal salarioliquido = (salarioBase + horasextra10 + drs20 - inss10 - ir - vt10);

            return salarioliquido;
        }


    }
}
