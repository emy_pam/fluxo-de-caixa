﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO dto)
        {
           
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome  é obrigatório.");
            }
            if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CPF  é obrigatório.");
            }

            PedidoDataBase db = new PedidoDataBase();
            return db.Salvar(dto);
        }

        public List<PedidoConsultarView> Consultar(string Nome)
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Consultar(Nome);
        }


        public List<PedidoConsultarView> Listar()
        {
            PedidoDataBase db = new PedidoDataBase();
            return db.Listar();
        }













    }







} 