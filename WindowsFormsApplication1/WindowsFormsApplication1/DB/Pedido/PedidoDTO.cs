﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Pedido
{
    public class PedidoDTO
    {
       public int idPedido { get; set; }
       public string Quantidade { get; set; }
       public string Nome { get; set; }
       public string CPF { get; set; }
       public int Produtos_idProdutos { get; set; }


    }
}
