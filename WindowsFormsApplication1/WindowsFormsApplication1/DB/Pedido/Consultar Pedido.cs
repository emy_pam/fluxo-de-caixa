﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Pedido;

namespace WindowsFormsApplication1
{
    public partial class Consultar_Pedido : UserControl
    {
        public Consultar_Pedido()
        {
            InitializeComponent();
            CarregarGrid();
        }

        public void CarregarGrid()
        {

            PedidoBusiness business = new PedidoBusiness();
            List<PedidoConsultarView> dto = new List<PedidoConsultarView>();
            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = dto;

        }


        private void button2_Click(object sender, EventArgs e)
        {
            PedidoBusiness business = new PedidoBusiness();
            List<PedidoConsultarView> lista = business.Consultar(textBox2.Text.Trim());

            dgvPedidos.AutoGenerateColumns = false;
            dgvPedidos.DataSource = lista;

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Consultar_Pedido_Load(object sender, EventArgs e)
        {

        }

        private void dgvPedidos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 3)
            {
                PedidoConsultarView pedido = dgvPedidos.Rows[e.RowIndex].DataBoundItem as PedidoConsultarView;

                this.Hide();


            }
        }
    }
}

            
