﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Pedido
{
    class PedidoDataBase
    {
     

        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO Pedido (Data, IdProduto, Quantidade, Produtos_idProdutos, CPF, Nome ) 
                              VALUES (@IdProduto, @Quantidade, @Nome, @CPF,@Produtos_idProdutos )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("IdPedido", dto.idPedido));
            parms.Add(new MySqlParameter("Quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("Produtos_idProdutos", dto.Produtos_idProdutos));
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("CPF", dto.CPF));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public void Remover(int id)
        {
            string script = @"DELETE FROM Pedido WHERE idPedido = @idPedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idPedido", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM Pedido WHERE Pedido like @Pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("IdPedido", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();

                dto.idPedido = reader.GetInt32("IdPedido");
                dto.Quantidade = reader.GetString("Quantidade");
                dto.CPF = reader.GetString("CPF");
                dto.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");
                dto.Nome = reader.GetString("Nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<PedidoConsultarView> Listar()
        {
            string script = @"SELECT * FROM Pedido CONSULTA";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoConsultarView> lista = new List<PedidoConsultarView>();
            while (reader.Read())
            {
                PedidoConsultarView dto = new PedidoConsultarView();
                dto.idPedido = reader.GetInt32("idPedido");
                dto.Quantidade = reader.GetString("Quantidade");
                dto.Nome = reader.GetString("Nome");
                dto.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");


                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        }
}
