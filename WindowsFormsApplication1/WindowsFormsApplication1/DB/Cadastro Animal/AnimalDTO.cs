﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Base.Cadastro_Animal
{
   public class AnimalDTO
    
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Porte { get; set; }
        public string Raca { get; set; }
        public int  Idade { get; set; }
        public string Exigencias { get; set; }
        public string observacoes { get; set; }
       
      
    }
}
