﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Fornecedores;

namespace WindowsFormsApplication1.DB.Base.Cadastro_Animal
{
    class DataBase_Cadastro_Animal
    {

        public int Salvar(AnimalDTO dto)
        {

            string script = @"INSERT INTO Cadastro_Animal ( Nome, Porte, Raca, Idade, Exigencias, observacoes) 
                                   VALUES ( @Nome, @Porte, @Raca, @Idade, @Exigencias, @observacoes)";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Porte", dto.Porte));
            parms.Add(new MySqlParameter("Raca", dto.Raca));
            parms.Add(new MySqlParameter("Idade", dto.Idade));
            parms.Add(new MySqlParameter("Exigencias", dto.Exigencias));
            parms.Add(new MySqlParameter("observacoes", dto.observacoes));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(AnimalDTO dto)
        {

            string script = @"UPDATE Cadastro_Animal
                                 SET  
                                      Nome = @Nome,
                                      Porte = @Porte,
                                      Raca = @Raca
                                      Idade = @Idade,
                                      Exigencias = @Exigencias,
                                      observacoes = @observacoes,
                               WHERE Cadastro_Animal = @Cadastro_Animal";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Porte", dto.Porte));
            parms.Add(new MySqlParameter("Raca", dto.Raca));
            parms.Add(new MySqlParameter("Idade", dto.Idade));
            parms.Add(new MySqlParameter("Exigencias", dto.Exigencias));
            parms.Add(new MySqlParameter("observacoes", dto.observacoes));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(int id)
        {

            string script = @"DELETE FROM Cadastro_Animal WHERE idCadastro_Animal = @idCadastro_Animal";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idCadastro_Animal", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }














    }
}

    