﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Fluxo_Caixa
{
    class Consultar_fluxo_de_caixa
    {

        public int idfluxo_de_caixa { get; set; }
        public string data_entrada { get; set; }
        public string data_saida { get; set; }
        public decimal preco_entrada { get; set; }
        public decimal preco_saida { get; set; }
        public int Produtos_idProdutos { get; set; }

    }
}
