﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Fluxo_Caixa
{
    class Fluxo_caixaDatabase
    {


        public List<Consultar_fluxo_de_caixa> Consultar(DateTime inicio, DateTime fim)
        {


            string script = @"select * from Consultar_fluxo_de_caixa
            WHERE DataReferencia >= @data_entrada
                AND DataReferencia <= @data_saida;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("data_entrada", inicio));
            parms.Add(new MySqlParameter("data_saida", fim));


            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Consultar_fluxo_de_caixa> lista = new List<Consultar_fluxo_de_caixa>();

            while (reader.Read())
            {
                Consultar_fluxo_de_caixa dto = new Consultar_fluxo_de_caixa();

                dto.data_entrada = reader.GetString("data_entrada");
                dto.data_saida = reader.GetString("data_saida");
                dto.preco_entrada = reader.GetDecimal("preco_entrada");
                dto.preco_saida = reader.GetDecimal("preco_saida");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
        public List<Consultar_fluxo_de_caixa> Listar()
        {
            string script = @"select * from vw_consultar_fluxodecaixa";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Consultar_fluxo_de_caixa> lista = new List<Consultar_fluxo_de_caixa>();

            while (reader.Read())
            {
                Consultar_fluxo_de_caixa dto = new Consultar_fluxo_de_caixa();

                dto.data_entrada = reader.GetString("data_entrada");
                dto.data_saida = reader.GetString("data_saida");
                dto.preco_entrada = reader.GetDecimal("preco_entrada");
                dto.preco_saida = reader.GetDecimal("preco_saida");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
    
    }
        
}

