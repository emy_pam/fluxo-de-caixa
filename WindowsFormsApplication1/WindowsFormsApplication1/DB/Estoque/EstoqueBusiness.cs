﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Estoque
{
    class EstoqueBusiness
    {

        public int Salvar(EstoqueDTO dto)
        {

            if (dto.Produtos_idProdutos == 0)
            {
                throw new ArgumentException("Produto não foi reconhecido");
            }

            EstoqueDataBase db = new EstoqueDataBase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            db.Remover(id);
        }

        public List<EstoqueDTO> Consultar(string estoq)
        {
            EstoqueDataBase db = new EstoqueDataBase();

            return db.Consultar(estoq);
        }
        public List<EstoqueDTO> Listar()
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.Listar();
        }
        public void Alterar(EstoqueDTO dto)
        {

            EstoqueDataBase db = new EstoqueDataBase();
            db.Alterar(dto);
        }

        public List<Consultar_estoque> ConsultarEstoque(string produtoestoque)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.ConsultarEstoque(produtoestoque);
        }
        public Consultar_estoque ConsultarEstoqueView(string produtoestoque)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.ConsultarEstoqueView(produtoestoque);
        }

    }
}
