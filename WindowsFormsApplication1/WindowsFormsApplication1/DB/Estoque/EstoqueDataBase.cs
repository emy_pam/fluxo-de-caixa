﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Estoque
{
    class EstoqueDataBase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script =
                @"INSERT INTO tb_estoque
                (Produtos_idProdutos, Quantidade_produto)
                VALUES
                (@Produtos_idProdutos, @Quantidade_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Produtos_idProdutos", dto.Produtos_idProdutos));
            parms.Add(new MySqlParameter("Quantidade_produto", dto.Quantidade_produto));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE Produtos_idProdutos = @Produtos_idProdutos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idProdutos", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<EstoqueDTO> Consultar(string estoq)
        {

            string script =
                @"SELECT * FROM tb_estoque
                  WHERE idEstoque like @idEstoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idEstoque", "%" + estoq + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> fornecedores = new List<EstoqueDTO>();
            while (reader.Read())
            {

                EstoqueDTO novoestoq = new EstoqueDTO();

                novoestoq.idEstoque = reader.GetInt32("idEstoque");
                novoestoq.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");
                novoestoq.Quantidade_produto = reader.GetInt32("Quantidade_produto");

                fornecedores.Add(novoestoq);

            }
            reader.Close();
            return fornecedores;
        }

        public List<EstoqueDTO> Listar()
        {

            string script =
                @"SELECT * FROM tb_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<EstoqueDTO> fornecedores = new List<EstoqueDTO>();
            while (reader.Read())
            {

                EstoqueDTO novoestoq = new EstoqueDTO();

                novoestoq.idEstoque = reader.GetInt32("idEstoque");
                novoestoq.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");
                novoestoq.Quantidade_produto = reader.GetInt32("Quantidade_produto");

                fornecedores.Add(novoestoq);

            }
            reader.Close();
            return fornecedores;
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script =
            @"UPDATE tb_estoque
                 SET 
                  Quantidade_produto = @Quantidade_produto
                  
                  WHERE Produtos_idProdutos = @Produtos_idProdutos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produtocompra", dto.Produtos_idProdutos));
            parms.Add(new MySqlParameter("Quantidade_produto", dto.Quantidade_produto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }


        public List<Consultar_estoque> ConsultarEstoque(string produtoestoque)
        {

            string script =
                @"SELECT * FROM Consultar_estoque
                  WHERE  Nome_produto like @ Nome_produto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter(" Nome_produto", "%" + produtoestoque + "%"));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<Consultar_estoque> fornecedores = new List<Consultar_estoque>();
            while (reader.Read())
            {

                Consultar_estoque novoestoq = new Consultar_estoque();
                novoestoq.Nome_produto = reader.GetString("Nome_produto");
                novoestoq.Quantidade_produto = reader.GetInt32("Quantidade_produto");

                fornecedores.Add(novoestoq);

            }
            reader.Close();
            return fornecedores;
        }
        public Consultar_estoque ConsultarEstoqueView(string produtoestoque)
        {
            string script =
                @"SELECT * FROM Consultar_estoque
                  WHERE nm_produtocompra = @nm_produtocompra";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produtocompra", produtoestoque));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            Consultar_estoque fornecedores = new Consultar_estoque();
            while (reader.Read())
            {
                fornecedores.Nome_produto = reader.GetString("Nome_produto");
                fornecedores.Quantidade_produto = reader.GetInt32("Quantidade_produto");
            }
            reader.Close();
            return fornecedores;
        }

    }
}


