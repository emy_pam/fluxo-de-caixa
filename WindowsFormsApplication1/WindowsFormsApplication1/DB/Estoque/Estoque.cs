﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1.DB.Estoque
{
    public partial class Estoque : Form
    {
        BindingList<Consultar_estoque> produtosCarrinho = new BindingList<Consultar_estoque>();


        public Estoque()
        {
            InitializeComponent();
            ConfigurarGrid();
            CarregarCombos();
        }

        void CarregarCombos()
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            //Produtos_idProdutos.ValueMember = nameof(Consultar_estoque.Produtos_idProdutos);
            //Quantidade_produto.DisplayMember = nameof(Consultar_estoque.Quantidade_produto);
            //txtConsultarEstoque.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dgvEstoque.AutoGenerateColumns = false;
            //dgvEstoque.DataSource = ProdutoDTO ;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = cbProduto.SelectedItem as ProdutoDTO;
            EstoqueBusiness business = new EstoqueBusiness();
            Consultar_estoque view = business.ConsultarEstoqueView(dto.Nome);
            lblAtual.Text = view.Quantidade_produto.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                EstoqueBusiness business = new EstoqueBusiness();
                //List<Consultar_estoque> a = business.ConsultarEstoque(txtConsultarEstoque.Text);
                dgvEstoque.AutoGenerateColumns = false;
                //dgvEstoque.DataSource = ProdutoDTO;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: " + ex.Message);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
