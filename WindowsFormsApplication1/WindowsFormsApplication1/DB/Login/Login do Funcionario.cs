﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Funcionario;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
        private void EnviarMensagemErro(string mensagem)
        {
            MessageBox.Show(mensagem, "PetShop",
                     MessageBoxButtons.OK,
                     MessageBoxIcon.Error);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                FuncionarioBusiness business = new FuncionarioBusiness();
                FuncionarioDTO funcionario = business.Logar(txtUsuario.Text, txtSenha.Text);

                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    Inicial menu = new Inicial();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "PetShop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (Exception ex)
            {
                EnviarMensagemErro("Não foi possivel logar: " + ex.Message);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            CadastroFuncionario frm = new CadastroFuncionario();
            frm.Show();
            Hide(); 
        }
    }
}
