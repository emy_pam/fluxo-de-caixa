﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Servico
{
   public class ServicoDTO
    {
       public int  idServicos { get; set; }
       public string  Tipo_servicos { get; set; }
       public string Valor_servico { get; set; }
       public string Descricao { get; set; }
       public string Atendimento { get; set; }


    }
}
