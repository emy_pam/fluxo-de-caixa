﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Estoque;

namespace WindowsFormsApplication1
{
    public partial class Inicial : Form
    {
        public Inicial()
        {
            InitializeComponent();
            Verificarpermi();
            Bloaqueados();
        }

        public void Bloaqueados()
        {
        }

        void Verificarpermi()
        {
            //if (UserSession.UsuarioLogado.permicoes_adm == true)
            //{

            //    if (UserSession.UsuarioLogado.permicoes_funcionario == true)
            //    {
            //        button8.Enabled = false;
            //        button4.Enabled = false;
            //        button2.Enabled = false;
            //        button7.Enabled = false;
            //        button12.Enabled = false;

            //    }

            //}

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Agendamento tela = new Agendamento();
            tela.Show();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            UserControl2 tela = new UserControl2();
            TELA(tela);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UserControl2 tela = new UserControl2();
            tela.Show();
            Hide();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Folha_de_pagamento_2 tela = new Folha_de_pagamento_2();
            tela.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
           CadastroFuncionario tela = new CadastroFuncionario();
            tela.Show();
            Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form2 tela = new Form2();
            tela.Show();
        }
        private void TELA (UserControl control)
        {
            if (panel3.Controls.Count == 1)
                panel3.Controls.RemoveAt(0);
            panel3.Controls.Add(control);
        }
        private void button5_Click(object sender, EventArgs e)
        {
            

            Fornecedores tela = new Fornecedores();
            TELA(tela);
         }
       

        private void button10_Click(object sender, EventArgs e)
        {
            Ordem_de_Serviço tela = new Ordem_de_Serviço();
            TELA(tela);

        }

        private void Inicial_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Cadastro_de_Produto tela = new Cadastro_de_Produto();
            tela.Show();
        }

        private void TELA(Cadastro_de_Produto control)
        {
            if (panel3.Controls.Count == 1)
                panel3.Controls.RemoveAt(0);
            panel3.Controls.Add(control);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            
            Consultar_Produto tela = new Consultar_Produto();
            TELA(tela);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            UserControl3 tela = new UserControl3();
            TELA(tela);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //FluxoCaixa tela = new FluxoCaixa();
            //tela.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Estoque tela = new Estoque();
            tela.Show();
        }
    }
}
